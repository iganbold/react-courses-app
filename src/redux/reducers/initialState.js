// Centralize initial redux state
export default {
  courses: [],
  authors: [],
  apiCallsInPropress: 0
};
