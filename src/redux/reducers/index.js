import { combineReducers } from "redux";
import courses from "./courseReducer"; // courses
import authors from "./authorReducer";
import apiCallsInPropress from "./apiStatusReducer";

const rootReducer = combineReducers({ courses, authors, apiCallsInPropress });

export default rootReducer;
