import React from "react";

const PageNoteFound = () => {
  return <h1>Oops! Page not found.</h1>;
};

export default PageNoteFound;
